import axios from 'axios';

const api = axios.create({
    baseURL: `https://xinabo.herokuapp.com`
});

export default api