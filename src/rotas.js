import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";

import Login from "./pages/login"
import Cadastrar from "./pages/login/cadastrar/cadastrar"
import Enviado from "./pages/login/cadastrar/esqueciSenha/enviado/enviado";
import Esqueci from "./pages/login/cadastrar/esqueciSenha/esqueci"
import Menu from "./pages/menu";
import NotFound from "./pages/notFound";
import Solicitacoes from "./pages/menu/solicitacoes";
import Coleta from "./pages/menu/coleta";
import ConsultarLixo from "./pages/menu/coleta/consultarLixo/consultarLixo";
import SolicitacaoEspecifica from "./pages/menu/solicitacoes/solicitacaoEspecifica";
import Perfil from "./pages/perfil";
import ConsultarSolicitacoes from "./pages/menu/solicitacoes/consultarSolicitacoes";
import Admin from "./pages/admin";

const Rotas = () => {

    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Login/>} />
                <Route path='/login' element={<Login/>} />
                <Route path='/cadastrar' element={<Cadastrar/>} />
                <Route path='/esqueci' element={<Esqueci/>}/>
                <Route path='/enviado' element={<Enviado/>}/>
                <Route path='/menu' element={<Menu/>}/>

                <Route path='/solicitacoes' element={<Solicitacoes/>}/>
                <Route path='/solicitacaoEspecifica/:tpSolicitacao' element={<SolicitacaoEspecifica/>}/>
                <Route path='/consultarSolicitacoes' element={<ConsultarSolicitacoes/>}/>
                

                <Route path='/coleta' element={<Coleta/>}/>
                <Route path='/consultarLixo/:bairro' element={<ConsultarLixo/>}/>
                
                <Route path='/perfil' element={<Perfil/>}/>
                <Route path='/admin' element={<Admin/>}/>
                
                <Route path='*' element={<NotFound/>} />
            </Routes>
        </BrowserRouter>
    )
}
export default Rotas;