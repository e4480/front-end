
import React, {} from 'react';
import {GoogleLogin} from 'react-google-login'

const clientId =  "55636480247-o6q4vmgv21ah75qac5vffjctf3m5b5i6.apps.googleusercontent.com"


function GoogleAuthButton(){

    function onSuccess(res){
        console.log('Deu certo', res.profileObj)
    }
    function onFailure(res){
        console.log('Deu Erro', res)
    }

    return (
        <div id="singInButton">
            <GoogleLogin 
            clientId={clientId}
            buttonText = "Gerar Lembrete"
            onSuccess={onSuccess}
            onFailure={onFailure}
            cookiePolicy={'single_host_origin'}
            isSignedIn={true}
            />
        </div>
    )
}

export default GoogleAuthButton


