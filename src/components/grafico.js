
import React, { useEffect, useState } from "react";
import { Chart } from 'react-google-charts';
import _ from 'lodash';

import api from '../api/api'
import Warning from "./warning";

const Grafico = () => {
    const [ charData, setChartData ] = useState([]);
    const [ options, setOpitions ] = useState({backgroundColor: 'transparent'});
    const [mensagemRetorno, setMensagemRetorno] = useState("Houve um erro. Tente novamente mais tarde.");
    const [statusRetorno, setStatusRetorno] = useState(true);
    const [isModalVisible, setIsModalVisible] = useState(false);

    const loadData = (data) => {
        const values = _.groupBy(data, (value) => value.tipo_lixo);

        console.log("values", values);

        const resultado = _.map(values, (value, key) => [key,
                _.sumBy(values[key], (v) => v.quantidade)]);

        return [
            ["Tipo de Lixo", "Quantidade"], ...resultado
        ]
    }

    useEffect(() => {
        GetInformacaoGrafico()

    }, []);

    function isSuccess() {
        setIsModalVisible(false)
        if (!statusRetorno) {
            window.location.href = '/coleta'//Redireciona para página de menu
        }
    }

    function GetInformacaoGrafico(){

        const config = {
            headers: { Authorization: `Bearer ${localStorage.getItem("access_token")}` }
        };

        api.get(`/solicitacoes/informacoes_grafico`, config).then(function (response) {//Recebe resposta
            console.log(response.data);

            if (!response.data.success) {//Se obteve sucesso
                setMensagemRetorno(response.data.message)
                setStatusRetorno(false)
                setIsModalVisible(true)
            } else {
                setChartData(loadData(response.data.model));
            }
        }).catch(function (error) {
            console.log(error);
        });
    }


    return(
        <div>
            <div>
                {isModalVisible ? (
                    <Warning onClose={() => isSuccess()}>
                        {mensagemRetorno}
                    </Warning>
                ) : null}
            </div>


            <Chart
                chartType="PieChart"
                data={charData}
                width={"100%"}
                height={"400px"}
                className="grafico"
                options={options}
                legendToggle
            />
        </div>
    );
};

export default Grafico