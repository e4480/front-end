
import React from 'react';

const Info = ({ id = "info", onClose = () => {}, children }) => {

    const handleOutsideClick = (e) => {
        if(e.target.id == id) onClose();
    }

    return <div id={id} className='modal' onClick={handleOutsideClick}>
        <div className='container-info'>
            <button className='close-X' onClick={onClose}>+</button><br/>
            <div className='content'>{children}</div>
        </div>
    </div>;
}

export default Info