
import React, { useState } from 'react';
import SwipeableViews from 'react-swipeable-views/lib/SwipeableViews';

import deletar from "../assets/deletar.png";

import api from '../api/api'
import Warning from "./warning";

const CardSolicitacoes = (props) => {
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [mensagemRetorno, setMensagemRetorno] = useState("Houve um erro. Tente novamente mais tarde.");
    const [statusRetorno, setStatusRetorno] = useState(true);

    function DeletarSolicitacao() {

        const config = {
            headers: { Authorization: `Bearer ${localStorage.getItem("access_token")}` }
        };

        api.delete(`/solicitacoes/cancelar/${props.cod_solicitacao}`, config).then(function (response) {//Recebe resposta
            console.log(response.data);
            setMensagemRetorno(response.data.message)
            setIsModalVisible(true)
            if (response.data.success) {//Se obteve sucesso
                setStatusRetorno(true)
            }
        }).catch(function (error) {
            console.log(error);
        });

    }

    function isSuccess() {
        setIsModalVisible(false)
        window.location.reload();

    }

    return (

        <div id={props.cod_solicitacao}>
            <div>
                {isModalVisible ? (
                    <Warning onClose={() => isSuccess()}>
                        {mensagemRetorno}
                    </Warning>
                ) : null}
            </div>

            <SwipeableViews enableMouseEvents>
                <div className='input-solicitation'>
                    <text className='menu-text-options-minor-cs'>Tipo de Solicitação: {props.tipo_solicitacao} </text><br />
                    <text className='menu-text-options-minor-cs'>Data de Agendamento: {props.data_agendamento} </text><br />
                    <text className='menu-text-options-minor-cs'>Endereço: {props.endereco} </text>
                </div>
                <div>
                    <button onClick={DeletarSolicitacao} className='delete-solicitation'><img className='img-deletar' alt="Deletar" src={deletar}></img></button>
                </div>
            </SwipeableViews>

        </div>
    )
}

export default CardSolicitacoes