
import React from 'react';

const CardColeta = (props) => {

    return (
        <div id={props.id_coleta}>

            <div className='input'> 
                <text className='menu-text-options-minor'>Tipo de coleta: {props.tipo_coleta} </text><br/>
                <text className='menu-text-options-minor'>Bairro: {props.bairro} </text><br/>
                <text className='menu-text-options-minor'>Horário: {props.horario} </text><br/>
                <text className='menu-text-options-minor'>Dia da semana: {props.dia_semana} </text> 
            </div>
      </div>
    )
}

export default CardColeta