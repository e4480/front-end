
import React from 'react';

const Warning = ({ id = " warning", onClose = () => {}, children }) => {

    const handleOutsideClick = (e) => {
        if(e.target.id == id) onClose();
    }

    return <div id={id} className='modal' onClick={handleOutsideClick}>
        <div className='container-warning'>
            <div className='content'>{children}</div>
            <button className='close-OK' onClick={onClose}>OK</button>   
        </div>
    </div>;
}

export default Warning