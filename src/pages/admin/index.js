import React, { useState } from 'react';

import logo from "../../assets/gomi.png";

import Warning from "../../components/warning.js";
import "../perfil/index.css";
import Grafico from '../../components/grafico';

function Admin() {
    if (!localStorage.getItem("userAdmin")) {
        window.location.href = '/'
    }

    const [isModalVisible, setIsModalVisible] = useState(false);

    function logout() {
        localStorage.clear();
        window.location.href = '/'
    }

    return (
        <div>
            <div>
                {isModalVisible ? (
                    <Warning onClose={() => setIsModalVisible(false)}>
                        Algo deu errado!<br />
                        Por favor tente novamente.
                    </Warning>
                ) : null}
            </div>


            {/* Cabeçalho e Imagem-Perfil */}
            <div className='menu-div-primary'>
                <img className='logo' src={logo} alt="xinabo"></img><br />
            </div>

            <p className='text-solicitation'>Admin</p>
            <div className='input-solicitation'>
                <text className='menu-text-options'>Gráfico</text>

                <Grafico />
            </div>
            <button onClick={logout} className='btn-exit'>Sair</button>
        </div>

    )
}


export default Admin