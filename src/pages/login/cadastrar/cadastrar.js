import React, { useState } from 'react';
import { Link } from 'react-router-dom';

import "../cadastrar/cadastrar.css";
import api from "../../../api/api";

import Warning from "../../../components/warning.js";
import seta2 from "../../../assets/login/seta-2.png"
import logo from "../../../assets/gomi.png"

function Cadastrar() {
    const [mensagem, setMensagem] = useState("");
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [mensagemRetorno, setMensagemRetorno] = useState("Houve um erro. Tente novamente mais tarde.");
    const [statusRetorno, setStatusRetorno] = useState(false);

    const [data, setData] = useState({
        nome: "",
        email: "",
        cpf: "",
        senha: "",
        confirmarSenha: "",
    })
    function handle(e) {
        const newData = { ...data }
        newData[e.target.id] = e.target.value
        setData(newData)
        console.log(newData)
    }

    function cadastrarPost() {
        if (data.nome == "" || data.senha == "" || data.email == "") {
            setMensagem("Campos obrigatórios estão vazios.")
        }
        else {
            setMensagem("")
            if (data.senha == data.confirmarSenha) {
                setMensagem("")
                api.post('/users/cadastrar_usuario', {
                    "nome": data.nome,
                    "email": data.email,
                    "cpf": data.cpf,
                    "senha": data.senha

                }).then(function (response) {
                    console.log(response.data);
                    setMensagemRetorno(response.data.message)
                    setIsModalVisible(true)
                    if (response.data.success) {
                        setStatusRetorno(true)
                    }
                })
                    .catch(function (error) {
                        console.log(error);
                    });
            } else {
                setMensagem("Senhas não batem.")
            }
        }
    }

    function isSuccess() {
        setIsModalVisible(false)
        if (statusRetorno) {
            window.location.href = '/login'
        }
    }

    return (
        <div>
            <div>
                {isModalVisible ? (
                    <Warning onClose={() => isSuccess()}>
                        {mensagemRetorno}
                    </Warning>
                ) : null}
            </div>

            {/* Cabeçalho - btn-return/Logo */}
            <div>
                <Link to='/login'> <button className='btn-return'><img className="seta-2" src={seta2}></img><text className='text-return'>Voltar</text></button></Link> <br />
                <img className='logo' src={logo}></img>
            </div>

            {/* Cadastrar */}
            <div>
                <form>
                    <input onChange={(e) => handle(e)} id="nome" value={data.nome} className='input' type="text" placeholder='Nome'></input><br />
                    <input onChange={(e) => handle(e)} id="email" value={data.email} className='input' type="text" placeholder='E-mail'></input><br />
                    <input onChange={(e) => handle(e)} id="cpf" value={data.cpf} className='input' type="text" placeholder='CPF (Opcional)'></input><br />
                    <input onChange={(e) => handle(e)} id="senha" value={data.senha} className='input' type="password" placeholder='Senha'></input>
                    <input onChange={(e) => handle(e)} id="confirmarSenha" value={data.confirmarSenha} className='input' type="password" placeholder='Confirmar Senha'></input>
                    <text>{mensagem}</text>
                </form>
                <button onClick={cadastrarPost} className='btn-primary' /*onClick={cadastrarPost}*/>Cadastrar</button>
            </div>
        </div>
    )
}


export default Cadastrar