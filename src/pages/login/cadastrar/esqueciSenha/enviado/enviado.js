import React from 'react';
import { Link } from 'react-router-dom';

import "../enviado/enviado.css";
import seta2 from "../../../../../assets/login/seta-2.png";

function Enviado(){

    return (
        <div>

            {/* Cabeçalho - btn-return */}
           <div>
                <Link to='/esqueci'><button className='btn-return'><img className="seta-2" src={seta2}></img><text className='text-return'>Voltar</text></button></Link>
            </div>

            {/* Recuperaçãp de senha */}
            <div>
                <p className='text-forgotPassword-sendEmail'>Maravilha. A restauração da senha foi enviado com sucesso para seu E-mail.</p>

                <Link to='/login'><button className='btn-primary'>Voltar a tela de login</button></Link>
            </div>
        </div>
    )
}


export default Enviado