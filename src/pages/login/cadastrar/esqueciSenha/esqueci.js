import React from 'react';
import { Link } from 'react-router-dom';

import "../esqueciSenha/esqueci.css";

import seta2 from "../../../../assets/login/seta-2.png";

function Esqueci(){

    return (
        <div>

           {/* Cabeçalho - btn-return */}
           <div>
                <Link to='/login'> <button className='btn-return'><img className="seta-2" src={seta2}></img><text className='text-return'>Voltar</text></button></Link>
            </div>

            {/* Cadastrar */}
            <div>
                <p className='text-forgotPassword-sendEmail'>Informe seu E-mail que lhe enviaremos uma recuperaçã ode senha :)</p>

                <input className='input' type="text" placeholder='E-mail'></input><br/>
                <input className='input' type="text" placeholder='Confirmar E-mail'></input><br/>

                <Link to='/enviado'><button className='btn-primary'>Enviar</button></Link>
            </div>
            
        </div>
    )
}


export default Esqueci