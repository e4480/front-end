import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import "../login/login.css";


import api from '../../api/api'

import logo from "../../assets/gomi.png";


function Login() {
    const [mensagem, setMensagem] = useState("");

    //Para tratar dinâmicamente os valores dos inputs
    const [data, setData] = useState({
        login: "",
        senha: "",
    })
    function handle(e) {
        const newData = { ...data }
        newData[e.target.id] = e.target.value
        setData(newData)
        console.log(newData)
    }

    function loginPost() {

        api.post('/users/login', {//Faz requisição para o back
            "email": data.login,
            "senha": data.senha

        }).then(function (response) {//Recebe resposta
            console.log(response.data);

            if (response.data.success) {//Se obteve sucesso
                let rota = ''
                if (response.data.flg_admin) {
                    rota = '/admin'
                } else {
                    rota = '/menu'
                }
                setMensagem("")
                localStorage.setItem("access_token", `${response.data.access_token}`) //Salva access_token no localStorage
                localStorage.setItem("userAuth", true)
                localStorage.setItem("userAdmin", response.data.flg_admin)

                localStorage.setItem("email_user", response.data.user.email)
                localStorage.setItem("nome_user", response.data.user.nome)

                console.log(rota)

                window.location.href = rota //Redireciona para página
            } else {
                setMensagem(response.data.message)
            }
        }).catch(function (error) {
            console.log(error);
        });
    }

    return (
        <div>

            {/* Cabeçalho - Logo */}
            <div><br /><br /><br /><br />
                <img className='logo' src={logo}></img><br /><br /><br /><br />
            </div>

            {/* Login */}
            <div>
                <form>
                    <input onChange={(e) => handle(e)} id="login" value={data.login} className='input' type="text" placeholder='Login'></input><br />
                    <input onChange={(e) => handle(e)} id="senha" value={data.senha} className='input' type="password" placeholder='Senha'></input>

                </form>
                <button className='btn-primary' onClick={loginPost}>Entrar</button>
                <text>{mensagem}</text>
            </div>
            {/* Botões - Cadastrar e Esqueci a senha */}
            <div>
                <Link to='/cadastrar'><button className='btn-register-fogot-password'><text className='text-return'>Cadastrar</text></button></Link><br />
                {/*<Link to='/esqueci'><button className='btn-register-fogot-password'><text className='text-return'>Esqueci a senha</text></button></Link>*/}
            </div>

        </div>
    )
}


export default Login