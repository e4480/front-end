import React, {useState} from 'react';
import { Link } from 'react-router-dom';

import logo from "../../../assets/gomi.png";
import seta2 from "../../../assets/login/seta-2.png";
import "../coleta/index.css";

function Coleta(){
    const [mensagem, setMensagem] = useState("")
    const [data, setData] = useState({
        bairro: ""
    })
    function handle(e) {
        const newData = { ...data }
        newData[e.target.id] = e.target.value
        setData(newData)
        console.log(newData)
    }
    
    function consultarLixo(){
        const bairro = data.bairro.replace(/ /g, "");
        console.log(bairro)
        console.log(bairro.length)
        if(bairro.length == 0){
            setMensagem("Um bairro deve ser informado.")
        }else{
            setMensagem("")
            window.location.href = `/consultarLixo/${data.bairro}`
        }
    }

    return (
        <div>
            {/* Cabeçalho e Logo */}
            <Link to='/menu'><button className='btn-return'><img className="seta-2" src={seta2}></img><text className='text-return'>Voltar</text></button><br/></Link>

            <div className='menu-div-primary'>
                <img className='logo-menor' src={logo}></img><br/><br/>
            </div>

            {/* Input e button  */}
            <div>
                <p className='text-solicitation'>Consulta de Lixo</p>
                <input onChange={(e) => handle(e)} id="bairro" value={data.bairro} className='input' placeholder='Bairro'></input>
                <button className='btn-primary'  onClick={consultarLixo}>Consultar</button>
            </div>
            <p>{mensagem}</p>

        </div>
    )
}


export default Coleta