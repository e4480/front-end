import React, { useState, useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { gapi } from 'gapi-script'

import logo from "../../../../assets/gomi.png";
import seta2 from "../../../../assets/login/seta-2.png";
import CardColeta from '../../../../components/cardColeta';
import "../consultarLixo/consultarLixo.css";

import api from '../../../../api/api'
import Warning from "../../../../components/warning.js";

import GoogleAuthButton from '../../../../components/googleAuth'

const clientId = "55636480247-o6q4vmgv21ah75qac5vffjctf3m5b5i6.apps.googleusercontent.com"
const apiKey = "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"
const DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"]
const SCOPES = "https://www.googleapis.com/auth/calendar.events"


function ConsultarLixo() {

    const [isModalVisible, setIsModalVisible] = useState(false);
    const [mensagemRetorno, setMensagemRetorno] = useState("Houve um erro. Tente novamente mais tarde.");
    const [statusRetorno, setStatusRetorno] = useState(true);
    const [cardColeta, setCards] = useState([])

    const { bairro } = useParams();

    useEffect(() => {
        function start() {
            gapi.client.init({
                apiKey: clientId,
                clientId: apiKey,
                discoveryDocs: DISCOVERY_DOCS,
                scope: SCOPES,
            })

            gapi.client.load('calendar', 'v3', () => console.log('bam!'))

            gapi.auth2.getAuthInstance().signIn()
                .then(() => {

                    var event = {
                        'summary': 'Awesome Event!',
                        'location': '800 Howard St., San Francisco, CA 94103',
                        'description': 'Really great refreshments',
                        'start': {
                            'dateTime': '2022-06-28T09:00:00-07:00',
                            'timeZone': 'America/Los_Angeles'
                        },
                        'end': {
                            'dateTime': '2022-06-28T17:00:00-07:00',
                            'timeZone': 'America/Los_Angeles'
                        },
                        'recurrence': [
                            'RRULE:FREQ=DAILY;COUNT=2'
                        ],
                        'attendees': [
                            { 'email': 'lpage@example.com' },
                            { 'email': 'sbrin@example.com' }
                        ],
                        'reminders': {
                            'useDefault': false,
                            'overrides': [
                                { 'method': 'email', 'minutes': 24 * 60 },
                                { 'method': 'popup', 'minutes': 10 }
                            ]
                        }
                    }

                    var request = gapi.client.calendar.events.insert({
                        'calendarId': 'primary',
                        'resource': event,
                    })

                    request.execute(event => {
                        console.log(event)
                        window.open(event.htmlLink)
                    })
                })
        }

        gapi.load('client:auth2', start)
        GetColetas();
    }, [])

    function GetColetas() {
        const config = {
            headers: { Authorization: `Bearer ${localStorage.getItem("access_token")}` }
        };

        api.get(`/coleta/${bairro}`, config).then(function (response) {//Recebe resposta
            console.log(response.data);

            if (!response.data.success) {//Se obteve sucesso
                setMensagemRetorno(response.data.message)
                setStatusRetorno(false)
                setIsModalVisible(true)
            } else {
                let cards = []
                response.data.model.forEach(coleta => {

                    cards.push((<div><CardColeta id_coleta={coleta.id_coleta} bairro={coleta.bairro} tipo_coleta={coleta.tipo_coleta} horario={coleta.horario} dia_semana={coleta.dia_semana} /> </div>))
                });
                setCards(cards)
            }
        }).catch(function (error) {
            console.log(error);
        });
    }

    function isSuccess() {
        setIsModalVisible(false)
        if (!statusRetorno) {
            window.location.href = '/coleta'//Redireciona para página de menu
        }
    }
    if (!localStorage.getItem("userAuth")) {
        window.location.href = '/'
    }


    return (

        <div>
            <div>
                {isModalVisible ? (
                    <Warning onClose={() => isSuccess()}>
                        {mensagemRetorno}
                    </Warning>
                ) : null}
            </div>

            {/* Cabeçalho e Logo */}
            <Link to='/coleta'><button className='btn-return'><img className="seta-2" src={seta2}></img><text className='text-return'>Voltar</text></button><br /></Link>

            <div className='menu-div-primary'>
                <img className='logo-menor' src={logo}></img><br /><br />
            </div>

            {/* Infos  */}
            <div>
                <p className='text-solicitation'> {bairro}</p><br />

                {<GoogleAuthButton className='btn-solicitation' />}

                {/*<CardColeta/>*/}
                {cardColeta}

            </div>

        </div>
    )
}


export default ConsultarLixo