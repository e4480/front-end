import React from 'react';
import { Link } from 'react-router-dom';

import "../menu/index.css";

import logo from "../../assets/gomi.png";
import perfil from "../../assets/menu/perfil.png";

function Menu() {

    if (!localStorage.getItem("userAuth")) {
        window.location.href = '/'
    }
    return (
        <div>

            {/* Cabeçalho e Logo */}
            <div className='menu-div-primary'>
                <img className='logo' src={logo}></img><br />
            </div>

            {/* Menu com as opções  */}
            <div className='menu-div'>
                <p className='text-solicitation'>Menu</p>

                <Link to='/coleta'><button className='btn-menu-options'><text className='menu-text-options'>Coleta</text></button></Link>

                <Link to='/solicitacoes'><button className='btn-menu-options'><text className='menu-text-options'>Solicitações</text></button></Link>

                <Link params={{ tpSolicitacao: 3 }} to='/perfil'><button className='btn-menu-options'><img className='menu-profile-icon' src={perfil}></img></button></Link>

            </div>

        </div>
    )
}


export default Menu