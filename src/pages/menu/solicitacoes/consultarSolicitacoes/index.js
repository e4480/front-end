import React, { View, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import CardSolicitacoes from '../../../../components/cardSolicitacoes';

import api from '../../../../api/api'
import Warning from "../../../../components/warning.js";

import logo from "../../../../assets/gomi.png";
import seta2 from "../../../../assets/login/seta-2.png";
import "../../solicitacoes/index.css";
import "../../../menu/index.css"

function ConsultarSolicitacoes() {
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [mensagemRetorno, setMensagemRetorno] = useState("Houve um erro. Tente novamente mais tarde.");
    const [statusRetorno, setStatusRetorno] = useState(true);
    const [cardSolicitacao, setCards] = useState([])

    useEffect(() => {
        GetSolicitacoes();
    }, [])

    function GetSolicitacoes() {
        const config = {
            headers: { Authorization: `Bearer ${localStorage.getItem("access_token")}` }
        };

        api.get('/solicitacoes/user', config).then(function (response) {//Recebe resposta
            console.log(response.data);

            if (!response.data.success) {//Se obteve sucesso
                setMensagemRetorno(response.data.message)
                setStatusRetorno(false)
                setIsModalVisible(true)
            } else {
                let cards = []
                response.data.model.forEach(solicitacao => {
                    let datahora = solicitacao.data_agendamento.split('T')
                    let data = datahora[0].split('-')
                    let dataformatada = `${data[2]}/${data[1]}/${data[0]}`
                  
                    cards.push((<div><CardSolicitacoes 
                                    cod_solicitacao={solicitacao.cod_solicitacao}
                                    tipo_solicitacao={solicitacao.tipo_solicitacao}
                                    endereco={solicitacao.endereco}
                                    data_agendamento={dataformatada} />
                    </div>))
                });
                setCards(cards)
            }
        }).catch(function (error) {
            console.log(error);
        });
    }

    function isSuccess() {
        setIsModalVisible(false)
        if (!statusRetorno) {
            window.location.href = '/menu'//Redireciona para página de menu
        }
    }
    if (!localStorage.getItem("userAuth")) {
        window.location.href = '/'
    }

    return (
        <div>
            <div>
                {isModalVisible ? (
                    <Warning onClose={() => isSuccess()}>
                        {mensagemRetorno}
                    </Warning>
                ) : null}
            </div>

            {/* Cabeçalho e Logo */}
            <Link to='/solicitacoes'><button className='btn-return'><img className="seta-2" src={seta2}></img><text className='text-return'>Voltar</text></button></Link>

            <div className='menu-div-primary'>
                <img className='logo-menor' src={logo}></img><br /><br />
            </div>


            {/* Button  */}
            <div>
                <p className='text-solicitation'>Lista de solicitação:</p>


                {/* <CardSolicitacoes />*/}
                {cardSolicitacao}

                {/* <text className='menu-text-options-minor'>Tipo de Solicitação: {data.tipo_solicitacao} </text><br/>
                    <text className='menu-text-options-minor'>Data de Agendamento: {data.data_agendamento} </text><br/>
                    <text className='menu-text-options-minor'>Endereço: {data.endereco} </text> 
                </CardSolicitacoes>*/}


            </div>

        </div>
    )
}

export default ConsultarSolicitacoes