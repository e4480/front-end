
import React, { useState, useEffect } from 'react';

import { Link, useParams } from 'react-router-dom';

import logo from "../../../../assets/gomi.png";
import seta2 from "../../../../assets/login/seta-2.png";
import info from "../../../../assets/info-2.png";

import api from '../../../../api/api'

import Warning from "../../../../components/warning.js";
import "../../solicitacoes/index.css";
import "../solicitacaoEspecifica/index.css";
import Info from '../../../../components/cardInformacao';


function SolicitacaoEspecifica() {
    const [mensagem, setMensagem] = useState("")
    const [isModalVisible, setIsModalVisible] = useState (false);
    const [isModalVisibleW, setIsModalVisibleW] = useState(false);
    const [mensagemRetorno, setMensagemRetorno] = useState("Houve um erro. Tente novamente mais tarde.");
    const [statusRetorno, setStatusRetorno] = useState(false);
    
    const [titulo, setTitulo] = useState("");
    const [descricao, setDescricao] = useState("");

    //Para tratar dinâmicamente os valores dos inputs
    const [data, setData] = useState({
        rua: "",
        numero: "",
        bairro: "",
        data: ""
    })
    function handle(e) {
        const newData = { ...data }
        newData[e.target.id] = e.target.value
        setData(newData)
        console.log(newData)
    }
    const { tpSolicitacao } = useParams();

    function PostSolicitacao() {

        if (data.rua == "" || data.numero == "" || data.bairro == "" || data.data == "") {
            setMensagem("Campos obrigatórios estão vazios.")
        } else {

            const config = {
                headers: { Authorization: `Bearer ${localStorage.getItem("access_token")}` }
            };

            const body = {
                "tipo_solicitacao": tpSolicitacao,
                "endereco": {
                    "rua": data.rua,
                    "numero": data.numero,
                    "bairro": data.bairro
                },
                "data_agendamento": data.data
            };

            api.post('/solicitacoes/cadastrar', body, config).then(function (response) {//Recebe resposta
                console.log(response.data);
                setMensagemRetorno(response.data.message)
                setIsModalVisibleW(true)
                if (response.data.success) {//Se obteve sucesso
                    setStatusRetorno(true)
                }
            }).catch(function (error) {
                console.log(error);
            });
        }
    }

    useEffect(() => {
        GetTiposSolicitacoes();
    }, [])

    function GetTiposSolicitacoes() {

        api.get(`/solicitacoes/tipo/${tpSolicitacao}`).then(function (response) {//Recebe resposta
            console.log(response.data);

            if (!response.data.success) {//Se obteve sucesso
                setMensagemRetorno(response.data.message)
                setStatusRetorno(false)
                setIsModalVisible(true)
            } else {
                setTitulo(response.data.model[0]['titulo_solicitacao'])
                setDescricao(response.data.model[0]['descricao'])
            }
        }).catch(function (error) {
            console.log(error);
        });

    }

    function isSuccess() {
        setIsModalVisibleW(false)
        if (statusRetorno) {
            window.location.href = '/menu'//Redireciona para página de menu
        }
    }

    if (!localStorage.getItem("userAuth")) {
        window.location.href = '/'
    }

    return (

        <div>
            <div>
                {isModalVisibleW ? (
                    <Warning onClose={() => isSuccess()}>
                        {mensagemRetorno}
                    </Warning>
                ) : null}
            </div>

            {/* Cabeçalho e Logo */}
            <Link to='/solicitacoes'><button className='btn-return'><img className="seta-2" src={seta2}></img><text className='text-return'>Voltar</text></button><br/></Link>
            
            <img className="info-img" src={info} onClick={() => setIsModalVisible(true)}></img>
                {isModalVisible ? (
                    <Info onClose={() => setIsModalVisible(false)}> 
                    <h2>{titulo}</h2>
                    <p>{descricao}</p>
                    </Info>
                ) : null}

            <div className='menu-div-primary'>
                <img className='logo-menor' src={logo}></img><br /><br />
            </div>

            {/* Input e button  */}
            <div>
                <p className='text-solicitation'>Fazer solicitação</p>

                <textarea cols="30" rows="5" className='input-solicitacao' name="solicitacaoEspecifica" placeholder='Rua' onChange={(e) => handle(e)} id="rua" value={data.rua}>
                    <input className='input-solicitacao' type="text" ></input>
                </textarea>

                <textarea cols="30" rows="5" className='input-solicitacao' name="solicitacaoEspecifica" placeholder='Número' onChange={(e) => handle(e)} id="numero" value={data.numero}>
                    <input className='input-solicitacao' type="text" ></input>
                </textarea>

                <textarea cols="30" rows="5" className='input-solicitacao' name="solicitacaoEspecifica" placeholder='Bairro' onChange={(e) => handle(e)} id="bairro" value={data.bairro}>
                    <input className='input-solicitacao' type="text" ></input>
                </textarea>

                <input className="input-solicitacao" type="date" onChange={(e) => handle(e)} id="data" value={data.data}></input>


                <button onClick={PostSolicitacao} className='btn-primary'>Gerar Solicitação</button>
                <text>{mensagem}</text>
            </div>

        </div>
    )
}


export default SolicitacaoEspecifica