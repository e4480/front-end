import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
// import Icon from 'react-native-vector-icons/FontAwesome';

import api from '../../../api/api'
import Warning from "../../../components/warning";

import logo from "../../../assets/gomi.png";
import seta2 from "../../../assets/login/seta-2.png";
import "../solicitacoes/index.css";



function Solicitacoes() {
    if (!localStorage.getItem("userAuth")) {
        window.location.href = '/'
    }
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [mensagemRetorno, setMensagemRetorno] = useState("Houve um erro. Tente novamente mais tarde.");
    const [statusRetorno, setStatusRetorno] = useState(true);
    const [cardSolicitacao, setCards] = useState([])

    useEffect(() => {
        GetTiposSolicitacoes();
    }, [])

    function GetTiposSolicitacoes() {

        api.get('/solicitacoes/tipo').then(function (response) {//Recebe resposta
            console.log(response.data);

            if (!response.data.success) {//Se obteve sucesso
                setMensagemRetorno(response.data.message)
                setStatusRetorno(false)
                setIsModalVisible(true)
            } else {
                let cards = []
                response.data.model.forEach(solicitacao => {
                    let path = `/solicitacaoEspecifica/${solicitacao.id_solicitacao}`
                    cards.push((<Link to={path} params={{ titulo: solicitacao.titulo_solicitacao, descricao: solicitacao.descricao}}><button className='btn-solicitation'><text className='text-solicitation-button'>{solicitacao.titulo_solicitacao}</text></button></Link>))
                });
                setCards(cards)
            }
        }).catch(function (error) {
            console.log(error);
        });

    }

    function isSuccess() {
        setIsModalVisible(false)
        if (!statusRetorno) {
            window.location.href = '/menu'//Redireciona para página de menu
        }
    }

    return (
        <div>
            <div>
                {isModalVisible ? (
                    <Warning onClose={() => isSuccess()}>
                        {mensagemRetorno}
                    </Warning>
                ) : null}
            </div>

            {/* Cabeçalho e Logo */}
            <Link to='/menu'><button className='btn-return'><img className="seta-2" src={seta2}></img><text className='text-return'>Voltar</text></button><br /></Link>

            <div className='menu-div-primary'>
                <img className='logo' src={logo}></img><br /><br />
            </div>

            {/* Input e button  */}
            <div>
                <p className='text-solicitation'>Solicitações</p>
                {cardSolicitacao}
                {/*<Link to='/solicitacaoEspecifica/1'><button className='btn-solicitation'><text className='text-solicitation-button'>Entulho</text></button></Link>
                <Link to='/solicitacaoEspecifica/2'><button className='btn-solicitation'><text className='text-solicitation-button'>Vegetais</text></button></Link>
                <Link to='/solicitacaoEspecifica/3'><button className='btn-solicitation'><text className='text-solicitation-button'>Caliças</text></button></Link>*/}


                <Link to='/consultarSolicitacoes'><button className='btn-solicitation'><text className='text-solicitation-button'>Consultar Solicitações</text></button></Link>
            </div>

        </div>
    )
}


export default Solicitacoes