import React, { useState } from 'react';
import { Link } from 'react-router-dom';

import perfil from "../../assets/menu/perfil2.png";
import seta2 from "../../assets/login/seta-2.png";


import "../perfil/index.css";


function Perfil() {

   // const [isModalVisible, setIsModalVisible] = useState(false);

    if (!localStorage.getItem("userAuth")) {
        window.location.href = '/'
    }

    function logout() {
        localStorage.clear();
        window.location.href = '/'
    }
    return (
        <div>
            {/*<div>
                {isModalVisible ? (
                    <Warning onClose={() => setIsModalVisible(false)}> 
                    Algo deu errado!<br/>
                    Por favor tente novamente. 
                    </Warning>
                ) : null}
            </div>*/}


            {/* Cabeçalho e Imagem-Perfil */}
            <Link to='/menu'><button className='btn-return'><img className="seta-2" src={seta2} alt="<"></img><text className='text-return'>Voltar</text></button><br /></Link>

            <div className='menu-div-primary'>
                <img className='profile' src={perfil} alt="Perfil"></img><br /><br />
            </div>

            {/* Button  */}
            <div>

                <button className='btn-menu-options'>
                    <text className='profile-infos'>Nome:  {localStorage.getItem("nome_user")}</text><br />
                    <text className='profile-infos'>E-mail:  {localStorage.getItem("email_user")}</text><br />
                </button>
                <button onClick={logout} className='btn-exit'>Sair</button>
            </div>
        </div>
    )
}


export default Perfil